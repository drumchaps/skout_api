
import requests
from json.decoder import JSONDecoder
import traceback


# noinspection SpellCheckingInspection
class SkoutAPI:
    
    END_POINTS = {
        "signin": "www.skout.com/api/1/auth/register",
        "profile": "http://www.skout.com/api/1/me",
        "update_profile": "http://www.skout.com/api/1/me/update",
        "upload_photo": "http://www.skout.com/api/1/me/upload_and_set_profile_image",
        "barriers": "www.skout.com/api/1/barriers",
        "check_mail": "https://www.skout.com/api/1/auth/register",
        "login": "http://www.skout.com/api/1/auth/login"
    }

    interested_strings = {
        0: "male",
        1: "female",
        2: "both",
    }
    ethnicity_strings = {
        0: "latin",
        1: "african",
        2: "native",
        3: "asian",
        4: "east_indian",
        5: "pacific",
        6: "white",
        7: "middle_eastern",
        8: "mixed",
    }
    looking_for_strings = {
        1: "friends",
        2: "flirting",
        3: "fun",
        4: "dating",
        5: "chat",
        6: "party",
        7: "relationship",
    }

    body_type_strings = {
        1: "slim",
        2: "petite",
        3: "slender",
        4: "normal",
        5: "more_to_love",
        6: "curvy",
        7: "swimmers_build",
        8: "muscular",
        9: "skinny",
        10: "tall",
        11: "athletic",
        12: "large_and_solid",
    }

    interests_strings = {
        1: "movies",
        2: "books",
        3: "tv",
        4: "music",
        5: "family",
        6: "pets",
        7: "drinking",
        8: "restaurants",
        9: "shopping",
        10: "watching_sports",
        11: "playing_sports",
        12: "bars",
        13: "dancing",
        14: "games",
    }

    relationship_strings = {
        0: "single",
        1: "in_a_relationship",
        2: "married",
        3: "its_complicated",
        4: "in_an_open_relationship",
        5: "separated",
        6: "divorced",
        7: "civil_union",
    }

    def __init__(self, username, password):
        ###
        # API DATA
        ###
        self.username = username
        self.password = password
        self.session_id = None

        ###
        # Profile Data
        ###
        self.name = None
        self.about = None

        self.ethnicity = None
        self.relationship = None
        self.interested_in = None

        self.body_types = []
        self.interests = []
        self.looking_fors = []

        ###
        # Requests data
        ###
        self.login_response = None
        self.proxies = None
        self.login_data = None
        self.login_error = None
        self.user_agent = None
        self.relationship = None
        self.interests = None

        self.photo_error = None
        self.photo_data = None
        self.photo_response = None
        self.update_profile_response = None
        self.get_profile_response = None
        self.barriers_response = None

        self.do_login_response = None

        self.check_mail_response = None
        pass

    def set_from_options(self, option, options_dict, attrname):
        """ Sets an attribute value from a class dict of options. \
        """
        if option in options_dict:
            setattr(self, attrname, option)
        pass

    def set_relationship_status(self, relationship):
        """ Tries to set the relationship status from the class dict of options \ 
        """
        self.set_from_options(relationship, self.relationship_strings, "relationship")
        pass

    def set_ethnicity(self, ethnicity):
        """ Tries to set the ethnicity value from a dict of options """
        self.set_from_options(ethnicity, self.ethnicity_strings)
        pass

    def set_interested_in(self, interested_in):
        """Tries to set the interested in value from a dict of options """
        
        self.set_from_options(interested_in, self.interested_strings)
        pass

    @staticmethod
    def add_attribute(attr, container_dict, obj_dict):
        """ Adds an option to an instance's array of options from a class dict of options. \
        """
        if attr in container_dict and attr not in obj_dict:
            obj_dict.append(attr)
        pass

    def add_interest(self, interest):
        """Tries to add an interest to the instance interests"""
        self.add_attribute(interest, self.interests_strings, self.interests)

    def add_body_type(self, body_type):
        """ Tries to add a body type to the instance body types"""
        self.add_attribute(body_type, self.body_type_strings, self.body_types)
        pass

    def add_looking_fors(self, looking_for):
        """ Tries to add a looking for to the instance looking fors"""
        self.add_looking_for(looking_for, self.looking_for_strings, self.looking_fors)
        pass

    def set_user_agent(self, user_agent):
        self.user_agent = user_agent
        pass

    """
    def set_profile(self):
        '''Sets the info about the profile in case the account is going\
        to be uploaded.
        '''
        pass
    """

    def do_request(
        self, method, endpoint,
        data=None, proxies=None, headers=None, files=None
    ):
        """ General function for building and sending data from other functions that
        make an http request.
        """
        if not proxies:
            proxies = self.proxies
        if method == "POST":
            request = requests.post
        if method == "GET":
            request = requests.get
        response = request(
            endpoint,
            headers=headers,
            data=data,
            proxies=proxies,
            files=files
        )
        return response

    def set_proxies(self, proxy):
        """ Set the global proxies for this account instance. \
        """
        self.proxies = {"http": proxy, "https": proxy}
        pass

    def set_secure(self, secure ):
        """Sets the secure option in case an endpoint is permitted to be\
        through http or https.\
        """
        self.secure = secure
        pass

    def login(self):
        """Builds, sends and formats the response for checking a successful login\
        """
        self.login_response = self.do_request(
            self.END_POINTS["signin"],
            data={
                "user": self.username,
                "password": self.password
            }
        )
        self.parse_login()
        pass

    def parse_login(self):
        """ Checks out the correct data is included in the json response """
        try:
            self.login_data = JSONDecoder().decode(self.login_response.text)
            if "status_code" in self.login_data and "session_id" in self.login_data:
                if (
                    self.login_data["status_code"] != 0 and 
                    self.login_data["status_code"] != 53
                ):
                    return False, self.login_data
                self.session_id = self.login_data["session_id"]
                return True, self.login_data
        except Exception, e:
            self.login_error = e
            return False, self.login_data    
        return False, self.login_data
        pass

    def do_login(self):
        # host = self.ENDPOINTS["login"]
        # "http://www.skout.com/api/1/auth/login"
        self.do_login_response = self.do_request(
            "POST",
            self.END_POINTS["login"],
            data = {
                "user": self.username,
                "pass": self.password
            }
        )
        return self.parse_do_login()

    def parse_do_login(self):
        try:
            json = JSONDecoder().decode(self.do_login_response.text)
            if "status_code" in json and "session_id" in json:
                if json["status_code"] != 0 and json["status_code"] != 53:
                    print "LOGIN UNSUCCESSFUL"
                    print json["status_code"]
                    return False, json
                self.session_id = json["session_id"]
                return True, json
        except Exception, e:
            print e
            print traceback.print_exc()
            raise e
            """
            print e
            print traceback.print_exc()
            return False, (e, traceback)
            """
        return False, json 

    def set_barriers(self):
        """
        """
        heads = {"session_id": self.session_id}
        self.barriers_response = self.do_request(
            self.ENDPOINTS["barriers"],
            proxies=self.proxies,
            headers=heads
        )
        return self.barriers_response

    def get_interested_string(self):
        self.interested_strings[self.interested_in]
        pass

    def build_profile_dict(self):
        data = {
            "about": self.about,
            "name": self.name,
            "username": self.username,
            "gender": self.gender,
            #Singulars
            "interested_in": self.get_interested_string(),
            "ethnicity": self.get_ethnicity_string(),
            "relationship_status": self.get_relationship_string(),
            #Plurals
            "body_type": self.get_,
            "looking_for": self.get_looking_for_string(),
            "interests": "",
        }
        return data

    def signin(self):
        """ Tries to sign in a new account with its profile data. """

        self.signin_response = self.do_request(
            self.ENDPOINTS["signin"],
            data = {
                "":""
            },
            proxies = self.proxies
        )
        pass

    def set_profile(self):
        """  """
        data = {
        }
        headers = {"session_id": self.session_id}
        self.profile_response = self.do_requests(
            "POST",
            self.ENDPOINTS["profile"],
            proxies=self.proxies,
            headers=headers,
            data=data
        )
        return self.profile_response

    def upload_photo(self, photo_path ):
        headers = {"session_id": self.session_id}
        with open(photo_path, "rb") as f:
            files = {
                "file": f
            }
            self.photo_response = self.do_request(
                "POST",
                self.END_POINTS["upload_photo"],
                headers=headers,
                files=files,
            )
        pass

    def parse_photo_upload(self):
        try:
            # self.photo_data = JSONDecoder().decode(self.photo_response)
            self.photo_data = self.photo_response.json()
            if "status" in self.photo_data:
                if self.photo_data["status"] != "success":
                    return False, self.photo_data
                return True, self.photo_data
            pass
        except Exception, e:
            self.photo_error = e
            print self.photo_error
            print self.photo_response.text
            return False, None
        return False, self.photo_data

    def get_profile(self):
        self.get_profile_response = self.do_request(
            "GET",
            self.END_POINTS["profile"],
            headers={"session_id": self.session_id},
        )
        return self.get_profile_response

    def update_profile(self, data):
        self.update_profile_response = self.do_request(
            "POST",
            self.END_POINTS["update_profile"],
            headers={"session_id": self.session_id},
            data=data
        )
        return self.update_profile_response

    def check_email_exists(
        self, mail, name, password="q1w2e3r4t5y", birthday_epoch="", gender="female", interested_in="", proxy=None
    ):
        proxies = None
        if proxy:
            proxies = {"http": proxy, "https": proxy}
        data = {
            "user": mail,
            "name": name,
            "pass": password,
            "birthday": birthday_epoch,
            "gender": gender,
            "interested_in": interested_in
        }
        self.check_mail_response = requests.post(
            self.END_POINTS["check_mail"],
            data = data,
            proxies = proxies
        )
        pass

    def parse_check_mail_exists(self):
        jsonresponse = self.check_mail_response.json()
        if "already exists" in jsonresponse["messages"] or jsonresponse["status_code"] == 1:
            return False
        return True
