from distutils.core import setup

# noinspection SpellCheckingInspection
setup(
    name="skout_api",
    version="0.0.1",
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/skout_api",
    packages=[
        "skout_api",
    ],
    package_dir={'skout_api': 'src/skout_api'}, requires=['requests']
)
